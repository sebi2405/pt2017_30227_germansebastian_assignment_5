import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.omg.Messaging.SyncScopeHelper;
import java.util.stream.*;
public class MonitoredData  {
	public String startTime;
	public String endTime;
	public String startDate;
	public String endDate;
	public String activityLabel;
	public Scanner x;
	public static List<MonitoredData> monitoredData=new ArrayList<>();
	
	public MonitoredData(String startTime, String endTime,String startDate, String endDate, String activityLabel){
		this.startTime=startTime;
		this.endTime=endTime;
		this.activityLabel=activityLabel;
		this.startDate=startDate;
		this.endDate=endDate;
	}
	public MonitoredData(){}
	
	
	public void readFile(){
		try{
			 x=new Scanner(new File("Activities.txt"));
		 }catch (Exception e){
			 System.out.println("fisier negasit");
		 }
		 while(x.hasNext()){
			 String a=x.next();
			 String b=x.next();
			 String c=x.next();
			 String e=x.next();
			 String f=x.next();
			 MonitoredData d=new MonitoredData(b,e,a,c,f);
			 monitoredData.add(d);
		 }
	}
	public void afis(){
		for(int i=0; i<monitoredData.size(); i++)
			System.out.println(monitoredData.get(i).startTime+" "+monitoredData.get(i).endTime+" "+monitoredData.get(i).activityLabel);
	}
	
	public String getDate(){
		return this.startDate;
	}
	
	public int numarare(){
		
		monitoredData.stream()
		.distinct();
		int j=1;
		for(int i=0; i<monitoredData.size()-1; i++){
			if(monitoredData.get(i).getDate().equals(monitoredData.get(i+1).getDate())==false)
				j++;
		}
		return j;
	}
	
	public void cerinta2(){
		
		Map <String,Integer> data=new HashMap<>();
		for(int i=0; i<monitoredData.size(); i++)
			data.put(monitoredData.get(i).activityLabel, 0);
		
		for(int i=0; i<monitoredData.size(); i++){
			data.put(monitoredData.get(i).activityLabel, (Integer)data.get(monitoredData.get(i).activityLabel)+1);
		}
		
		
		try{
			File file=new File("cerinta2.txt");
			PrintWriter outputStream=new PrintWriter(file);
			Set setOfKeys=data.keySet();
			Iterator i=setOfKeys.iterator();
			while(i.hasNext()){
				String key=(String)i.next();
				Integer val=(Integer)data.get(key);
				outputStream.println(key+" "+val);
			}
			outputStream.close();
		}catch (Exception e){
			System.out.println("nu s-a putut deschide fisierul");
		}
	}
	
	public void cerinta3(){
		Map<String,Map<String,Integer>> data=new HashMap<>();
		Map<String,Integer> dt=new HashMap<>();
		for(int i=0; i<monitoredData.size()-1; i++){
			try{
				dt.put(monitoredData.get(i).activityLabel, (Integer)dt.get(monitoredData.get(i).activityLabel)+1);
			}catch (Exception e){
				dt.put(monitoredData.get(i).activityLabel, 1);
			}
			
			if(monitoredData.get(i).startDate.equals(monitoredData.get(i+1).startDate)==false){
			   data.put(monitoredData.get(i).startDate, dt);
			   dt=new HashMap<>();
			}
		}
		try{
			File file=new File("cerinta3.txt");
			PrintWriter outputStream=new PrintWriter(file);
			Set setOfKeys=data.keySet();
			Iterator i=setOfKeys.iterator();
			while(i.hasNext()){
				String key=(String)i.next();
				Map<String,Integer> val=(HashMap)data.get(key);
				outputStream.println(key);
				Set set2=val.keySet();
				Iterator j=set2.iterator();
				while(j.hasNext()){
					String c=(String)j.next();
					Integer v=(Integer)val.get(c);
					outputStream.println("  "+c+" "+v);
				}
			}
			outputStream.close();
		}catch (Exception e){
			System.out.println("nu s-a putut deschide fisierul");
		}
		
	}
	
	public void cerinta4() {
		Map<String,Long> date=new HashMap<>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		for(int i=0; i<monitoredData.size(); i++){
			String d1=monitoredData.get(i).startDate+" "+monitoredData.get(i).startTime;
			String d2=monitoredData.get(i).endDate+" "+monitoredData.get(i).endTime;
			Date date1=null,date2 = null;
			try{
				 date1=sdf.parse(d1);
		    	 date2=sdf.parse(d2);
			}catch (Exception e){
				System.out.println("conversie imposibila");
			}
			long dif=(date2.getTime()-date1.getTime())/1000;
			try{
				date.put(monitoredData.get(i).activityLabel, (Long)date.get(monitoredData.get(i).activityLabel)+dif);
			}catch (Exception e){
				date.put(monitoredData.get(i).activityLabel, dif);
			}
			
		} 
		
		try{
			File file=new File("cerinta4.txt");
			PrintWriter outputStream=new PrintWriter(file);
			Set setOfKeys=date.keySet();
			Iterator i=setOfKeys.iterator();
			while(i.hasNext()){
				String key=(String)i.next();
				Long val=(Long)date.get(key);
				if(val>36000){
				  long h=val/3600;
				  long m=(val%3600)/60;
				  long s=(val%3600)%60;
				  outputStream.println(key+" "+val+" secunde  ("+h+":"+m+":"+s+")");
				}
			}
			outputStream.close();
		}catch (Exception e){
			System.out.println("nu s-a putut deschide fisierul");
		}
		
	}
	public void cerinta5(){
		Map<String,Integer> date=new HashMap<>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		for(int i=0; i<monitoredData.size(); i++){
			String d1=monitoredData.get(i).startDate+" "+monitoredData.get(i).startTime;
			String d2=monitoredData.get(i).endDate+" "+monitoredData.get(i).endTime;
			Date date1=null,date2 = null;
			try{
				 date1=sdf.parse(d1);
		    	 date2=sdf.parse(d2);
			}catch (Exception e){
				System.out.println("conversie imposibila");
			}
			long dif=(date2.getTime()-date1.getTime())/1000;
			try{
				if(dif>=300)
				date.put(monitoredData.get(i).activityLabel, (Integer)date.get(monitoredData.get(i).activityLabel)+1);
			}catch (Exception e){
				if(dif>=300)
				    date.put(monitoredData.get(i).activityLabel, 1);
				else
					date.put(monitoredData.get(i).activityLabel,0);
			}
		}
		Map<String,Integer> total=new HashMap<>();
		for(int i=0; i<monitoredData.size(); i++){
			try{
				total.put(monitoredData.get(i).activityLabel, (Integer)total.get(monitoredData.get(i).activityLabel)+1);
			}catch (Exception e){
				total.put(monitoredData.get(i).activityLabel, 1);
			}
		}
		try{
			File file=new File("cerinta5.txt");
			PrintWriter outputStream=new PrintWriter(file);
			Set setOfKeys=date.keySet();
			Iterator i=setOfKeys.iterator();
			while(i.hasNext()){
				String key=(String)i.next();
				Integer val=(Integer)date.get(key);
				Integer big=(Integer)total.get(key);
			
				if(val>=0.9*big)
				  outputStream.println(key+"--- nr total:"+big+", nr aparitii peste 5 min:"+val);
				
			}
			outputStream.close();
		}catch (Exception e){
			System.out.println("nu s-a putut deschide fisierul");
		}
	}
}

	
